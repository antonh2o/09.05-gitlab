# Домашнее задание к занятию "09.05 Gitlab"

## Подготовка к выполнению

1. Необходимо [зарегистрироваться](https://about.gitlab.com/free-trial/)
2. Создайте свой новый проект
3. Создайте новый репозиторий в gitlab, наполните его [файлами](./repository)
4. Проект должен быть публичным, остальные настройки по желанию

```
Ответ: зарегистрировал  https://gitlab.com/antonh2o/09.05-gitlab

```

## Основная часть

### DevOps

В репозитории содержится код проекта на python. Проект - RESTful API сервис. Ваша задача автоматизировать сборку образа с выполнением python-скрипта:
1. Образ собирается на основе [centos:7](https://hub.docker.com/_/centos?tab=tags&page=1&ordering=last_updated)
2. Python версии не ниже 3.7
3. Установлены зависимости: `flask` `flask-jsonpify` `flask-restful`
4. Создана директория `/python_api`
5. Скрипт из репозитория размещён в /python_api
6. Точка вызова: запуск скрипта
7. Если сборка происходит на ветке `master`: Образ должен пушится в docker registry вашего gitlab `python-api:latest`, иначе этот шаг нужно пропустить

```
Ответ:
Создан Dockerfile согласно заданию https://gitlab.com/antonh2o/09.05-gitlab/-/blob/master/Dockerfile
Автоматизация сборки в файле .gitlab-ci.yml https://gitlab.com/antonh2o/09.05-gitlab/-/blob/master/.gitlab-ci.yml

```

### Product Owner

Вашему проекту нужна бизнесовая доработка: необходимо поменять JSON ответа на вызов метода GET `/rest/api/get_info`, необходимо создать Issue в котором указать:
1. Какой метод необходимо исправить
2. Текст с `{ "message": "Already started" }` на `{ "message": "Running"}`
3. Issue поставить label: feature

```
создан issue Product Owner to Developer
label: feature
```

### Developer

Вам пришел новый Issue на доработку, вам необходимо:
1. Создать отдельную ветку, связанную с этим issue
2. Внести изменения по тексту из задания
3. Подготовить Merge Requst, влить необходимые изменения в `master`, проверить, что сборка прошла успешно

```
Ответ:
Выполнено все 3 пукта по адресу
 https://gitlab.com/antonh2o/09.05-gitlab/-/commit/f9c697bae025af28a04698d9e5d63a03da08c75a
```
![fix_issue](img/fix_issue.png "fix_issue")

### Tester

Разработчики выполнили новый Issue, необходимо проверить валидность изменений:
1. Поднять докер-контейнер с образом `python-api:latest` и проверить возврат метода на корректность
2. Закрыть Issue с комментарием об успешности прохождения, указав желаемый результат и фактически достигнутый

```
Ответ:
docker pull registry.gitlab.com/antonh2o/09.05-gitlab:latest

~/# docker run -d --name python-api -p 5290:5290 registry.gitlab.com/antonh2o/09.05-gitlab:latest

Digest: sha256:4076a9d6902ec397a0d1f384ed3d302170184d085d2dbbf0369c2e6a84c7b726
Status: Downloaded newer image for registry.gitlab.com/antonh2o/09.05-gitlab:latest

~/# curl localhost:5290/get_info
{"version": 3, "method": "GET", "message": "Running"}
```
![get_info-fixed](img/get_info-fixed.png "get_info-fixed")
## Итог

После успешного прохождения всех ролей - отправьте ссылку на ваш проект в гитлаб, как решение домашнего задания
https://gitlab.com/antonh2o/09.05-gitlab



---

### Как оформить ДЗ?

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

---
